
function browserNotify(msg) {
  if (!("Notification" in window)) return;
  if (Notification.permission === "granted") {
    if (msg.indexOf('<br/>') >= 0) {
        var headBody = msg.split('<br/>', 2);
        var msgTitle = headBody[0].replace( /<.*?>/g, '' );
        var msgBody = headBody[1].replace( /<.*?>/g, '' );
        var notification = new Notification(
            msgTitle, { body: msgBody, icon: 'logo.png' }
        );
    }
  }
}

$("INPUT").val("")
$("#name").keypress(function(evt){
  if(evt.originalEvent.keyCode==13){
    $("#join").trigger("click")
  }
})

if (!("Notification" in window)) {
  //alert("This browser does not support desktop notification");
} else {
  if (Notification.permission !== 'denied') {
    Notification.requestPermission(function (permission) {
      // If the user accepts, let's create a notification
      if (permission === "granted") {
        var notification = new Notification("Notification popup activated.");
      }
    });
 }
}

//handling the start of the chat
$("#join").click(function(){
  $("#error").html("");
  var name= escapeHtml($("#name").val())
  if(name.length<3){
    $("#error").html("Tihis name is to short!");
    return
  }
  console.log("join started")
  chat = new WebSocket("ws://"+window.location.host+"/ws");
  chat.onopen = function(evt) {
    chat.send(name);  //sending the chat name
    $("#phase1").animate({opacity:0},500,"linear",function(){
      $("#phase1").css({display:"none"})
      $("#phase2").css({opacity:1})
      $("#msg").focus()
    })
  };
  chat.onerror = function(evt) { 
    console.log("Websocket Error:",evt) 
  };  
  chat.onclose = function(evt) {
    console.log("chat closing")
    $("#phase1").stop().css({display:"block"}).animate({opacity:1},500)
    $("#phase2").stop().animate({opacity:0})
    $("#error").html("Sorry, but this name is in use.")
  };
  chat.onmessage = function(evt) {
    $("#messages").prepend(evt.data);
    browserNotify(evt.data);
  };

})

$("#msg").keypress(function(evt){
  if(evt.originalEvent.keyCode==13 && !evt.originalEvent.shiftKey){
    $("#send").trigger("click")
    evt.preventDefault();
  }
});

$("#send").click(function(){
  chat.send(escapeHtml($("#msg").val()));
  $("#msg").val("");
});

//helper function for escaping HTML
var entityMap = {
  "&": "&amp;",
  "<": "&lt;",
  ">": "&gt;",
  '"': '&quot;',
  "'": '&#39;',
  "/": '&#x2F;',
  "\n": '<BR/>'
};

function escapeHtml(string) {
  return String(string).replace(/[&<>"'\/]/g, function (s) {
    return entityMap[s];
  });
}

//helper function for debugging purposes
function toHex(str) {
  var result = '';
  for (var i=0; i<str.length; i++) {
    result += ("0"+str.charCodeAt(i).toString(16)).slice(-2)+" ";
  }
  return result.toUpperCase();
}


