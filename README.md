# Comic-Chat

It is a very simple chat server written in Go. It broadcasts the messages
via websocket to every logged in user. It uses Browser notification, if it
is suppoted and allowed. NO ADDITIONAL EXTERNAL SERVICE IS REQUIRED! The
server is basically a webserver which serves the files necessary to run
the chat clients in a browser, plus it handles the chat traffic via websockets.

Every message is placed in a random comic image. This funny idea is from
the 90th. I played with perl guestbook code in the 90th.

## Usage

- Download or clone the project
- run the server on port 80 as root: `go run server.go`
- connect to the server from any computer in your local network: `http://_server_ip_or_name_:80`
- attention: many times port 80 is firewalled or only the admin is allow to run/bind to port 80

## root crontab -e

use `@reboot go run /path/to/the/server.go` as entry.

## Credits

- [indy golang](https://github.com/indy-golang/simple-chat) for the 5 years old code basics.
- [Fonts2u](https://fonts2u.com) for free non-google-webfonts!
- [gorilla websockets](https://github.com/gorilla/websocket)

![Screenshot Desktop](screenshot1.png)
![Screenshot Android](screenshot2.jpg)
